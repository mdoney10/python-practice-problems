# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) < 2:
        return None
    max_value = 0
    second_largest = 0
    for x in values:
        if x > max_value:
            second_largest = max_value
            max_value = x
        elif x > second_largest and x != max_value:
            second_largest = x
    return second_largest
